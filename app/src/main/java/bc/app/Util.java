package bc.app;

import android.content.ServiceConnection;
import androidx.annotation.NonNull;
import bc.app.util.Function0;

import java.util.Collection;
import java.util.Iterator;

public class Util {
  public static <T> Iterable<T> wrap(Iterator<T> iterator) {
    return () -> iterator;
  }

  private static <T> Iterable<T> wrap(T[] i) {
    return ()->new Iterator<T>(){
      int pos=0;
      @Override
      public boolean hasNext() {
        return pos<i.length;
      }

      @Override
      public T next() {
        return i[pos++];
      }

    };
  }

  public static <E, T extends E> void addAll(Collection<E> c, Iterator<T> i) {
    addAll(c, wrap(i));
  }
  public static <E, T extends E> void addAll(Collection<E> c, Iterable<T> i) {
    for (T t : i) {
      c.add(t);
    }
  }
  public static <E, T extends E> void addAll(@NonNull Collection<E> c, T[] i) {
    addAll(c,wrap(i));
  }

  public static <E, T extends E> void replaceAll(Collection<E> c, Iterator<T> i)
  {
    c.clear();
    addAll(c,i);
  }

  public static <E, T extends E> void replaceAll(@NonNull Collection<E> c, T[] i) {
    c.clear();
    addAll(c,i);
  }
  public static <E, T extends E> void replaceAll(Collection<E> c, Iterable<T> i)
  {
    c.clear();
    addAll(c, i);
  }
  public static void waitUntil(Object o, Function0<Boolean> cond) {
    waitUntil(o,cond,100);
  }
  public static void waitUntil(Object o, Function0<Boolean> cond, long timeout) {
      synchronized(o) {
      while (!cond.apply()) {
        wait(o,timeout);
      }
    }
  }
  @SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
  private static long wait(Object o, long timeout) {
    long endTime = System.currentTimeMillis()+timeout;
    synchronized(o) {
      try {
        o.wait(timeout);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    return endTime-System.currentTimeMillis();
  }
}
