package bc.app;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

@SuppressWarnings("unused")
public class Reflect {
  public static final String TAG = currentSimpleClassName();


  public static String currentSimpleClassName() {
    return currentSimpleClassName(1);
  }

  public static String currentMethodName() {
    return currentMethodName(1);
  }

  public static String currentMethodName(int i) {
    String res = currentStackPos(i).getMethodName();
    if(res.equals("<init>"))
      return currentSimpleClassName(i);
    else
      return res;
  }

  public static String currentSimpleClassName(int i) {
    String fullClassName = currentStackPos(i).getClassName();
    int pos = fullClassName.lastIndexOf('.')+1;
    return fullClassName.substring(pos);
  }

  public static StackTraceElement currentStackPos(int i) {
    Exception ex = new Exception();
    StackTraceElement[] trace = ex.getStackTrace();
    if (trace.length < 3 + i) {
      throw new RuntimeException("trace.length<" + (3 + i) + "!");
    }
    return trace[2 + i];
  }

  public static StackTraceElement currentStackPos() {
    return currentStackPos(1);
  }
  public static Method findStaticMethod(Class<?> clazz, String name) {
    for( Method method : clazz.getDeclaredMethods() ) {
      if(!method.getName().equals(name))
        continue;
      if(method.getTypeParameters().length != 0)
        continue;
      int modifiers = method.getModifiers();
      if((modifiers & Modifier.STATIC) == 0)
        continue;
      return method;
    }
    return null;
  }
}

