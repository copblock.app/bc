package bc.app;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.hardware.Camera;
import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import bc.api.ILiveVideoBroadcaster;
import bc.api.LiveVideoBroadcaster;
import bc.api.utils.Resolution;
import bc.api.utils.Utils;
import com.google.android.material.snackbar.Snackbar;
import org.jetbrains.annotations.NotNull;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

//
//@SuppressWarnings("deprecation")
@SuppressWarnings("deprecation")
public class BCFragment extends Fragment {
  public static final String                    TAG          = Utils.getTag();
  static              Handler                   smHandler    = new Handler(Looper.getMainLooper());
  private final       MainActivity              mMainActivity;
//  public              TimerHandler              mTimerHandler;
  private             boolean                   mIsRecording = false;
  private             boolean                   mIsMuted     = false;
//  private             Timer                     mTimer;
  private             CameraResolutionsFragment mCameraResolutionsDialog;
  private       ViewGroup                 mRootView;
  private       EditText                  mStreamNameEditText;
  private       ImageButton               mSettingsButton;
  private       TextView                  mStreamLiveStatus;
  GLSurfaceView             mGLView;


  private       Button                    mToggle;
  private       ImageButton               mMicToggle;
  private       ImageButton               mCamSwitch;
  private final View.OnClickListener      mOnClick    = this::onClick;
  private       ContentLoadingProgressBar mProgressBar;
  /**
   * Defines callbacks for service binding, passed to bindService()
   */


  public BCFragment(MainActivity mainActivity) {
    super(R.layout.activity_live_video_broadcaster);
    mMainActivity = mainActivity;
  }
  static void later(Runnable r) {
    smHandler.post(r);
  }
  private void triggerStartRecording() {
    if(mIsRecording)
      return;

    if(mMainActivity.getLiveVideoBroadcaster()==null) {
      mMainActivity.startService();
      later(this::triggerStartRecording);
    }

  }
  public void triggerStopRecording() {
    if (!mIsRecording)
      return;

    mToggle.setText(R.string.start_broadcasting);

    mStreamLiveStatus.setVisibility(View.GONE);
    mStreamLiveStatus.setText(R.string.live_indicator);
    mSettingsButton.setVisibility(View.VISIBLE);

    getLiveVideoBroadcaster().stopBroadcasting();
    mIsRecording = false;
  }
  @Override
  public void onStart() {
    Log.i(TAG, "onStart");
    super.onStart();
  }
  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    Log.i(TAG, "onViewCreated");
    super.onCreate(savedInstanceState);

//    mTimerHandler = new TimerHandler(getContext(), this);

    mStreamNameEditText = view.findViewById(R.id.stream_name_edit_text);

    mRootView       = view.findViewById(R.id.root_layout);
    mSettingsButton = view.findViewById(R.id.settings_button);
    mSettingsButton.setOnClickListener(mOnClick);
    mStreamLiveStatus = view.findViewById(R.id.stream_live_status);

    mToggle = view.findViewById(R.id.toggle_broadcasting);
    mToggle.setOnClickListener(mOnClick);

    mMicToggle = view.findViewById(R.id.mic_mute_button);
    mMicToggle.setOnClickListener(mOnClick);

    mCamSwitch = view.findViewById(R.id.switch_camera);
    mCamSwitch.setOnClickListener(mOnClick);

    mGLView = view.findViewById(R.id.cameraPreview_surfaceView);
    if (mGLView == null)
      throw new RuntimeException("No surfaceView");

    mGLView.setEGLContextClientVersion(2);     // select GLES 2.0
  }
  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                         @NonNull int[] grantResults)
  {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    if (requestCode == LiveVideoBroadcaster.PERMISSIONS_REQUEST) {
      if (getLiveVideoBroadcaster().isPermissionGranted()) {
        getLiveVideoBroadcaster().openCamera(Camera.CameraInfo.CAMERA_FACING_BACK);
      } else {
        if (ActivityCompat.shouldShowRequestPermissionRationale(mMainActivity,
                                                                Manifest.permission.CAMERA) ||
            ActivityCompat.shouldShowRequestPermissionRationale(mMainActivity,
                                                                Manifest.permission.RECORD_AUDIO)) {
          getLiveVideoBroadcaster().requestPermission();
        } else {
          AlertDialog.Builder builder = new AlertDialog.Builder(mMainActivity);
          builder.setTitle(R.string.permission)
                .setMessage(getString(R.string.app_doesnot_work_without_permissions))
                .setPositiveButton(android.R.string.yes,
                                   (dialog, which) ->
                {
                  try {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    intent.setData(Uri.parse(
                      "package:" + mMainActivity.getApplicationContext().getPackageName()));
                    startActivity(intent);
                  } catch (ActivityNotFoundException e) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
                    startActivity(intent);
                  }
                }).show();
        }
      }
    }
  }
  @Override
  public void onPause() {
    Log.i(TAG, "onPause");
    super.onPause();

    //hide dialog if visible not to create leaked window exception
    if (mCameraResolutionsDialog != null && mCameraResolutionsDialog.isVisible())
      mCameraResolutionsDialog.dismiss();

    if (getLiveVideoBroadcaster() != null)
      getLiveVideoBroadcaster().pause();
  }
  @Override
  public void onStop() {
    Log.i(TAG, "onStop");
    super.onStop();
  }
  @Override
  public void onConfigurationChanged(@NonNull Configuration newConfig) {
    super.onConfigurationChanged(newConfig);

    if(getLiveVideoBroadcaster()==null)
      return;

    if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE ||
        newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
      getLiveVideoBroadcaster().setDisplayOrientation();
    }

  }
  public void onClick(View v) {
    if (v == mMicToggle) {
      mIsMuted = !mIsMuted;
      getLiveVideoBroadcaster().setAudioEnable(!mIsMuted);
      int     drawable = mIsMuted ? R.drawable.ic_mic_mute_off_24 : R.drawable.ic_mic_mute_on_24;
      Context context  = getContext();
      assert context != null;
      Resources.Theme theme = context.getTheme();
      mMicToggle.setImageDrawable(ResourcesCompat.getDrawable(getResources(), drawable, theme));
    } else if (v == mToggle) {
      if (mIsRecording) {
        triggerStopRecording();
      } else {
        triggerStartRecording();
      }
    } else if (v == mCamSwitch) {
      if (getLiveVideoBroadcaster() != null) {
        getLiveVideoBroadcaster().changeCamera();
      }
    } else if (v == mSettingsButton) {
      FragmentActivity activity = getActivity();
      assert activity != null;
      FragmentManager     fm             = activity.getSupportFragmentManager();
      FragmentTransaction ft             = fm.beginTransaction();
      Fragment            fragmentDialog = fm.findFragmentByTag("dialog");
      if (fragmentDialog != null)
        ft.remove(fragmentDialog);

      ArrayList<Resolution> sizeList = getLiveVideoBroadcaster().getPreviewSizeList();
      Resolution            size     = getLiveVideoBroadcaster().getPreviewSize();

      if (sizeList != null && sizeList.size() > 0) {
        mCameraResolutionsDialog = new CameraResolutionsFragment(this::setResolution);

        mCameraResolutionsDialog.setCameraResolutions(sizeList, size);

        mCameraResolutionsDialog.show(getActivity().getSupportFragmentManager(),
                                      "resolution_dialog");
      } else {
        Snackbar.make(mRootView, "No resolution available", Snackbar.LENGTH_LONG).show();
      }
    }
  }

  private void sendToast(String text) {
    if (Looper.getMainLooper().isCurrentThread()) {
      Toast.makeText(getContext(), text, Toast.LENGTH_LONG).show();
    } else {
      later(() -> sendToast(text));
    }
  }
   public Void setResolution(Resolution size) {
    sendToast("New Resolution: " + size);

    if(getLiveVideoBroadcaster()!=null)
      getLiveVideoBroadcaster().setResolution(size);

    return null;
  }
  public ILiveVideoBroadcaster getLiveVideoBroadcaster() {
    return mMainActivity.getLiveVideoBroadcaster();
  }

//  private static class TimerHandler extends Handler {
//    static final int                       CONNECTION_LOST = 2;
//    static final int                       INCREASE_TIMER  = 1;
//    final        WeakReference<BCFragment> mFragmentRef;
//    final        WeakReference<Context>    mContextRef;
//    final        long                      sTime           = -System.currentTimeMillis();
//
//    TimerHandler(Context context, BCFragment fragment) {
//      super(Looper.getMainLooper());
//      mContextRef  = new WeakReference<>(context);
//      mFragmentRef = new WeakReference<>(fragment);
//    }
//
//    public void handleMessage(Message msg) {
//      Context    context  = mContextRef.get();
//      BCFragment fragment = mFragmentRef.get();
//      if (fragment == null) {
//        return;
//      }
//      if (context == null) {
//        return;
//      }
//      switch (msg.what) {
//        case INCREASE_TIMER:
//          String format = context.getString(R.string.live_indicator);
//          int elapsedTime = (int) ((sTime + System.currentTimeMillis()) / 1000);
//          String text = Utils.getDurationString(format, elapsedTime);
//          fragment.mStreamLiveStatus.setText(text);
//          break;
//        case CONNECTION_LOST:
//          fragment.triggerStopRecording();
//          new AlertDialog.Builder(context).setMessage(R.string.broadcast_connection_lost)
//                                          .setPositiveButton(android.R.string.yes, null).show();
//
//          break;
//      }
//    }
//  }

  private static class StringStringBooleanAsyncTask extends AsyncTask<String, String, Boolean> {
    WeakReference<BCFragment> mContext;

    private StringStringBooleanAsyncTask(BCFragment context) {
      System.out.println(Thread.currentThread());
      mContext = new WeakReference<>(context);
    }
    public ContentLoadingProgressBar getProgressBar() {
      return getFragment().mProgressBar;
    }
    public void setProgressBar(ContentLoadingProgressBar progressBar) {
      getFragment().mProgressBar = progressBar;
    }
    private Context getContext() {
      BCFragment fragment = getFragment();
      Context    context  = fragment.getContext();
      assert context != null;
      return context;
    }
    @NotNull
    private BCFragment getFragment() {
      BCFragment fragment = mContext.get();
      assert fragment != null;
      return fragment;
    }
    @Override
    protected void onPreExecute() {
      System.out.println(Thread.currentThread());
      Context context = getContext();
      assert context != null;
      setProgressBar(new ContentLoadingProgressBar(context));
      getProgressBar().show();
    }

    @Override
    protected Boolean doInBackground(String... url) {
      System.out.println(Thread.currentThread());
      long    eTime = -System.currentTimeMillis();
      Boolean res   = getFragment().getLiveVideoBroadcaster().startBroadcasting(url[0]);
      eTime += System.currentTimeMillis();
      String msg = String.format("Elapsed Time: %d", eTime);
      getFragment().sendToast(msg);
      return res;
    }

    @Override
    protected void onPostExecute(Boolean result) {
      System.out.println(Thread.currentThread());
      getProgressBar().hide();
      BCFragment fragment = getFragment();
      fragment.mIsRecording = result;
      if (result) {
        fragment.mStreamLiveStatus.setVisibility(View.VISIBLE);

        fragment.mToggle.setText(R.string.stop_broadcasting);
        fragment.mSettingsButton.setVisibility(View.GONE);
      } else {
        View root = fragment.mRootView;
        Snackbar.make(root, R.string.stream_not_started, Snackbar.LENGTH_LONG).show();

        fragment.triggerStopRecording();
      }
    }

  }
}
