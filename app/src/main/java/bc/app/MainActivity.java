package bc.app;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import bc.api.ILiveVideoBroadcaster;
import bc.api.LiveVideoBroadcaster;
import bc.app.util.Function0;
//import bc.app.liveVideoBroadcaster.LiveVideoBroadcasterActivity;

public class MainActivity extends AppCompatActivity {
  public static final String   RTMP_BASE_URL = "rtmp://dev.copblock.app:9999/cell411/";
  private final VBServiceConnection mConnection = new VBServiceConnection();
  class VBServiceConnection implements ServiceConnection
  {
    ILiveVideoBroadcaster mLiveVideoBroadcaster;
    @SuppressWarnings("deprecation")
    @Override
    public void onServiceConnected(ComponentName className, IBinder service)
    {
      // We've bound to LocalService, cast the IBinder and get LocalService
      // instance
      LiveVideoBroadcaster.LocalBinder binder = (LiveVideoBroadcaster.LocalBinder) service;
      if (mLiveVideoBroadcaster == null) {
        mLiveVideoBroadcaster = binder.getService();
        mLiveVideoBroadcaster.init(MainActivity.this, mBCFragment.mGLView);
        mLiveVideoBroadcaster.setAdaptiveStreaming(true);
      }
      mLiveVideoBroadcaster.openCamera(Camera.CameraInfo.CAMERA_FACING_FRONT);
    }

    @Override
    public void onServiceDisconnected(ComponentName arg0) {
      mLiveVideoBroadcaster = null;
    }
    public synchronized @Nullable ILiveVideoBroadcaster getLiveVideoBroadcaster() {
      return mLiveVideoBroadcaster;
    }
  };
  private final BCFragment        mBCFragment = new BCFragment(this);
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    requestWindowFeature(Window.FEATURE_NO_TITLE);
    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    setContentView(R.layout.activity_main);


    FragmentManager     fragmentManager = getSupportFragmentManager();
    FragmentTransaction trans           = fragmentManager.beginTransaction();
    trans.replace(R.id.fragment, mBCFragment);
    trans.commit();
  }
  @Override
  protected void onResume() {
    super.onResume();
    //binding on resume not to having leaked service connection
  }
  protected void startService() {
    Intent serviceIntent = new Intent(this, LiveVideoBroadcaster.class);
    //this makes service do its job until done
    startService(serviceIntent);
    bindService(serviceIntent, mConnection, 0);
  }
  protected void stopService() {
    unbindService(mConnection);
  }
  protected void onPause() {
    super.onPause();
  }
  public static final String TAG = Reflect.currentSimpleClassName();
  public ILiveVideoBroadcaster getLiveVideoBroadcaster() {
    try {
      Log.i(TAG, "enter getLiveVideoBroadcaster");
      return mConnection.getLiveVideoBroadcaster();
    } finally {
      Log.i(TAG, "exit getLiveVideoBroadcaster");
    }
  }
}
