This is an example program, originally based on the stock one, for

net.butterflytv.utils:rtmp-client

It was hacked from their example code.

I've mostly removed the deprecated stuff, and removed the player half
of the app.

It was developed as a POC for integrating said library into cell411.

Blame me for the bugs, unless you can reproduce them using official code.
